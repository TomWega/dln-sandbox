# Digital Lab in a Nutshell sandbox

## Cloning

You must clone the repo with submodules:
```sh
git clone --recurse-submodules git@gitlab.com:TomWega/dln-sandbox.git
```
This uses git submodules, they can sometimes be tricky to deal with so here is some guidance to keep you on track.

Initialize the yor local configuration file:
```sh
git submodule update --init --recursive
```
Fetch all the data from the project and check out the appropriate commit.
```sh
git submodule update --recursive
```

## Building

This project is built with maven. To build, simply run:
```sh
mvn clean install
```
To build without running the tests, you can run:
```sh
mvn clean install -DskipTests
```
