package sila_java.servers.examples.balance;

import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.balancecontroller.v1.BalanceControllerGrpc;
import sila2.org.silastandard.examples.balancecontroller.v1.BalanceControllerOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.types.SiLAErrors;
import sila_java.library.core.sila.types.SiLAReal;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;
import java.util.Random;

import static sila_java.library.core.utils.FileUtils.getResourceContent;
import static sila_java.library.core.utils.Utils.blockUntilStop;

@Slf4j
public class BalanceServer implements AutoCloseable {
    // Every SiLA Server needs to define a type
    public static final String SERVER_TYPE = "Balance Server";

    // SiLA Server constructed in constructor
    private final SiLAServer server;

    /**
     * Application Class using command line arguments
     * @param argumentHelper Custom Argument Helper
     */
    BalanceServer(@NonNull final ArgumentHelper argumentHelper) {

        /*
        Start the minimum SiLA Server with the Meta Information
        and the gRPC Server Implementations (found below)
          */
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "Balance Server",
                "www.wega-it.com",
                "v0.0"
        );

        try {
            /*
            A configuration file has to be given if the developer wants to persist server configurations
            (such as the generated UUID)
             */
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            /*
            Additional optional arguments are used, if no port is given it's automatically chosen, if
            no network interface is given, discovery is not enabled.
             */
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            builder.addFeature(
                    getResourceContent("BalanceController.sila.xml"),
                    new BalanceControllerImpl()
            );

            this.server = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            log.error("Something went wrong when building / starting server", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.server.close();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        try (final BalanceServer balanceServer = new BalanceServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(balanceServer::close));
            blockUntilStop();
        }

        log.info("termination complete.");
    }

    /**
     * Implementation of the gRPC Service mapped from the "BalanceController" Feature, the stubs
     * are automatically generated from the maven plugin.
     */
    static class BalanceControllerImpl extends BalanceControllerGrpc.BalanceControllerImplBase {
        double currentWeight = 0.0;

        /**
         */
        public void weigh(
                BalanceControllerOuterClass.Weigh_Parameters req,
                StreamObserver<BalanceControllerOuterClass.Weigh_Responses> res) {
            try {
                if (!req.hasTimeout()) {
                    res.onError(SiLAErrors.generateValidationError(
                            "Timeout",
                            "Timeout parameter was not set. Specify timeout in range of 0-10s"
                    ));
                }

                final long timeout = req.getTimeout().getValue();
                try {
                    Thread.sleep(timeout * 1000);
                } catch (InterruptedException e) {
                    // nothing to do
                }

                this.currentWeight = getRandomValue(1.48, 1.52);
                SiLAFramework.Real weight = SiLAReal.from((float)this.currentWeight);

                BalanceControllerOuterClass.Weigh_Responses result =
                        BalanceControllerOuterClass.Weigh_Responses
                                .newBuilder()
                                .setWeight(weight)
                                .build();
                res.onNext(result);
                res.onCompleted();
                System.out.println("Request received on " + SERVER_TYPE + " " + "weigh with timeout of " + timeout + "s");
            } catch (final Exception e) {
                res.onError(SiLAErrors.generateDefinedExecutionError(
                        "WeighError",
                        "Weight measurement failed due to unstable conditions")
                );
            }
        }

        /**
         */
        public void tare(
                BalanceControllerOuterClass.Tare_Parameters req,
                StreamObserver<BalanceControllerOuterClass.Tare_Responses> res) {
            try {
                this.currentWeight = 0.0;
                BalanceControllerOuterClass.Tare_Responses result =
                        BalanceControllerOuterClass.Tare_Responses
                                .newBuilder()
                                .setSuccess(SiLAString.from("OK"))
                                .build();
                res.onNext(result);
                res.onCompleted();
                System.out.println("Request received on " + SERVER_TYPE + " " + "tare");
            } catch (final Exception e) {
                log.error(e.getMessage());
                res.onError(SiLAErrors.generateGenericExecutionError(e));
            }
        }

        /**
         */
        public void getWeight(
                BalanceControllerOuterClass.Get_Weight_Parameters request,
                StreamObserver<BalanceControllerOuterClass.Get_Weight_Responses> res) {
            try {
                final double weight = this.currentWeight;
                res.onNext(
                        BalanceControllerOuterClass.Get_Weight_Responses
                                .newBuilder()
                                .setWeight(SiLAReal.from((float)weight))
                                .build()
                );
                res.onCompleted();
                System.out.println("Request received on " + SERVER_TYPE + " " + "weight");
            } catch (final Exception e) {
                log.error(e.getMessage());
                res.onError(SiLAErrors.generateGenericExecutionError(e));
            }
        }

        private double getRandomValue(double min, double max) {
            Random r = new Random();
            return min + (max - min) * r.nextDouble();
        }
    }
}

